#/bin/bash
#wget "http://bunwell.cs.ucl.ac.uk/ipinyou.contest.dataset.zip";
echo "File http://bunwell.cs.ucl.ac.uk/ipinyou.contest.dataset.zip was downloaded!"
# change to hdfs commands
hadoop fs -mkdir -p "$input";
echo "Directories were created!"
 
# get just filename with extension
url="ipinyou.contest.dataset.zip"
# get filename without extension
directory="ipinyou.contest.dataset"
# full path to .txt.bz2 files
path="ipinyou.contest.dataset/training3rd/imp.201310"
 
echo "Beginning or unzipping..."
unzip "$url"
echo "Archive was unzipped to ipinyou.contest.dataset"
for (( i=19; i<=27; i++));
do
    bzip2 -d "$path$i.txt.bz2";
    echo "$path$i.txt.bz2 unzipped!"
    #hdfs fs -put "$path$i.txt" "$input"
    hadoop fs -put "$path$i.txt" "$input";
    echo "$path$i.txt put in $input!"
done
hadoop fs -put "ipinyou.contest.dataset/city.en.txt";
rm -rf "ipinyou.contest.dataset";
echo "All files were copied to input folder"