package ru.alexandrovroman.mephi;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.WritableComparable;

public class CustomType implements WritableComparable<CustomType> {

    private  IntWritable price;


    // constructors
    public CustomType() {
    	this.price = new IntWritable(0);
    }
    
    public CustomType(int i) {
        this.price = new IntWritable(i);
    }

    public CustomType(String i) {
        this.price = new IntWritable(Integer.parseInt(i));
    }

    // getter
    public IntWritable getPrice() {
        return price;
    }

    
    // setter
    public void setPrice(IntWritable i) {
        this.price = i;
    }

    
    public void set(int i) {
    	this.price = new IntWritable(i);
    }

    @Override
    public void write(DataOutput out) throws IOException {
        price.write(out);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        price.readFields(in);
    }

    @Override
    public int hashCode() {
        return price.hashCode() * 123;
        
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof CustomType) {
            CustomType tp = (CustomType) o;
            return price.equals(tp.price);
        }
        return false;
    }

    @Override
    public String toString() {
	
      return Integer.toString(price.get());
	 }



    @Override
    public int compareTo(CustomType tp) {
 
	//int i1 = price.get();
	//int i2 = tp.getCount().get();
	
	//if (i1 == i2) {
		//return 0;
	//} else {return -1;}

    int cmp = price.compareTo(tp.price);
        if (cmp!=0) {
            return cmp;
        }
    return 0;

    }
}

