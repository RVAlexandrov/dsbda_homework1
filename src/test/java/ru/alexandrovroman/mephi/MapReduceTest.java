package ru.alexandrovroman.mephi;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.lang.System.out;
import static java.lang.System.setOut;


public class MapReduceTest {
    private Text testTextLine;
    private Text testCity;
    private MapDriver<LongWritable, Text, Text, CustomType> mapDriver;
    private ReduceDriver<Text, CustomType, Text, IntWritable> reduceDriver;
    private MapReduceDriver<LongWritable, Text, Text, CustomType, Text, IntWritable> mapReduceDriver;

    /**
     * init mapper, reducer, set path to txt file,
     * init test instances for mapper, reducer and mapreducer
     */
    @Before
    public void setUp() throws Exception {
        PriceMapper mapper = new PriceMapper();
        mapper.setPathToMap(getClass().getClassLoader().getResource("city.en.txt").getFile());
        PriceReducer reducer = new PriceReducer();
        mapDriver = MapDriver.newMapDriver(mapper);
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
        mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);

        testTextLine = new Text("954a6f233b9fa21450266813705cbcb4\t20131022092601326\t1\tDAM8_JBlaEb\tMozilla/5.0 (Windows NT 5.1) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.89 Safari/537.1\t110.204.200.*\t201\t204\t3\t3043163ba84753b4b51dd3290caeae67\t3d78ddb143e36ff72b73d5ef8d5c90a2\tnull\tALLINONE_F_Width1\t1000\t90\tNa\tNa\t70\t10734\t294\t79\tnull\t2821\tnull\n");
        testCity = new Text("xiangtan");
    }

    /**
     * need to pass input and output params to mapdriver,
     * run test
     */
    @Test
    public void testMapper() throws Exception {
        mapDriver.withInput(new LongWritable(1), testTextLine);
        mapDriver.withOutput(testCity, new CustomType(294));
        mapDriver.runTest();
    }

    /**
     * need to pass input and output params to reducedriver,
     * run test
     */
    @Test
    public void testReducer() throws Exception {
//        List <CustomType> values = new ArrayList<CustomType>();
//        values.add(new CustomType(250));
//        values.add(new CustomType(300));
//        values.add(new CustomType(251));
//        values.add(new CustomType(265));
//        values.add(new CustomType(50));
//        System.out.println(values);
    List values = Arrays.asList(new CustomType(250)
            ,new CustomType(300)
            ,new CustomType(251)
            ,new CustomType(265)
            ,new CustomType(50));
        System.out.println(values);
        reduceDriver.withInput(testCity, values);
        reduceDriver.withOutput(testCity, new IntWritable(3));
        reduceDriver.runTest();
    }

    /**
     * need to pass input and output params to mapreducedriver,
     * run test
     */
    @Test
    public void testMapReduce() throws Exception {
        mapReduceDriver.withInput(new LongWritable(1), testTextLine);
        mapReduceDriver.withInput(new LongWritable(2), testTextLine);
        mapReduceDriver.withInput(new LongWritable(3), testTextLine);
        mapReduceDriver.withOutput(testCity, new IntWritable(3));
        mapReduceDriver.runTest();
    }
}
