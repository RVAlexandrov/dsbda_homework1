package ru.alexandrovroman.mephi;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;


public class Driver extends Configured implements Tool{

    public static void main(String args[]) throws Exception {
        int exitCode = ToolRunner.run(new Driver(), args);
        System.exit(exitCode);
    }

    @Override
    public int run(String[] args) throws Exception {
        Configuration conf = getConf();
        
        // check number of args
        if (args.length != 2) {
            System.out.println("Usage: "+getClass().getSimpleName()+" needs two arguments: input and output directories\n");
            return -1;
        }
        
         //output format
        conf.set("mapreduce.output.textoutputformat.separator",",");
        
        // get job, set jar class and name
        Job job = Job.getInstance(conf);
        job.setJarByClass(Driver.class);
        job.setJobName("High bidding");

        // mapper params
        job.setMapperClass(PriceMapper.class);
        job.setMapOutputValueClass(CustomType.class);
        job.setMapOutputKeyClass(Text.class);

        // reducer params
        job.setReducerClass(PriceReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        job.setOutputFormatClass(TextOutputFormat.class);


        // set paths
        FileInputFormat.setInputPaths(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

	// remove old files
        FileSystem fs = FileSystem.get(conf);
        if (fs.exists(new Path(args[1])))
            fs.delete(new Path(args[1]), true);
        

        int returnValue = job.waitForCompletion(true) ? 0 : 1;
        if(job.isSuccessful()) {
            System.out.println("Job was successful done!");
        } else if(!job.isSuccessful()) {
            System.out.println("Job was not done!!!");
        }
        return returnValue;
    }
}
