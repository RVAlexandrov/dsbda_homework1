# Homework 1

DS:BDA Homework 1 repo

### Prerequisites (Cloudera)
- 64-bit VMs require a 64-bit host OS and a virtualization product that can support a 64-bit guest OS.
- The amount of RAM required by the VM: 8+ GiB 

### MANUAL

Build project: 
```sh
$ git clone git@bitbucket.org:rvalexandrov/dsbda_homework1.git
$ cd homework1
$ mvn clean install
```
Prepare data:
```sh
$ chmod +x prepare_data.sh
$ ./prepare_data.sh
```
Run:
```sh
$ cd target
$ hadoop jar homework1-0.0.1.jar ru.alexandrovroman.mephi.Driver input output
```
Show output:
```sh
$ hadoop fs -text output/*
```
