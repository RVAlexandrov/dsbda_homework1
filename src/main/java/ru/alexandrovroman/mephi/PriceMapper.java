package ru.alexandrovroman.mephi;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class PriceMapper extends Mapper<LongWritable, Text, Text, CustomType> {
    // hashmap <city_id, city_name>
    private Map<Integer, String> cityIdNameMap;
    // path to file with city_id and name_id
    private String pathToMap;

    /**
     * split input text line by tab,
     * get city name from hashmap, get value of price,
     * check city name and price,
     * write data to context
     *
     * @param key line num
     * @param value line text
     * @param context mapreduce context
     */
    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String[] split = value.toString().split("\t");
        String city = getCity(split[7].trim());
        Integer price = getPrice(split[19].trim());
        if (!city.equalsIgnoreCase("UNDEFINED") && price > 0) {
            context.write(new Text(city), new CustomType(price));
        }
    }

    /**
     * setup context,
     * create and fill with value hashmap
     *
     * @param context mapreduce context
     * @throws IOException
     * @throws InterruptedException
     */
    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        super.setup(context);
        cityIdNameMap = new HashMap<>();
        // not test
        if (pathToMap == null) {
            readFile(context);
        // test
        } else {
            readFileForTest();
        }
    }

    /**
     * from HDFS -> FOR MAPREDUCE
     * get path to file,
     * get FS,
     * read file line by line,
     * for each line: split line and put it to hashmap
     *
     * @param context mapreduce contex
     */
    private void readFile(Context context) throws IOException {
        Path path = new Path("city.en.txt");
        FileSystem fs = FileSystem.get(context.getConfiguration());
        try (BufferedReader in = new BufferedReader(new InputStreamReader(fs.open(path)))) {
            String line;
            line = in.readLine();
            while (line != null) {
                String[] parts = line.split("\\s+");
                cityIdNameMap.put(Integer.parseInt(parts[0].trim()), parts[1].trim());
                line = in.readLine();
            }
        }
    }

    /**
     * open local file,
     * read file line by line,
     * for each line: split line and put it to hashmap
     */
    private void readFileForTest() throws IOException {
        File file = new File(pathToMap);
        Scanner scanner = new Scanner(file);
        while (scanner.hasNextLine()) {
            String[] parts = scanner.nextLine().split("\\s+");
            cityIdNameMap.put(Integer.parseInt(parts[0].trim()), parts[1].trim());
        }
    }

    /**
     * get value of price in String
     *
     * @param price price in String
     * @return integer value of price if OK, '-1' - else
     */
    private Integer getPrice(String price) {
        try {
            return Integer.valueOf(price);
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    /**
     * get integer value of city_id,
     * if hashmap contains city_id -> return city, else -> UNDEFINED
     *
     * @param cityId city_id in String
     * @return City name if OK, 'UNDEFINED' - else
     */
    private String getCity(String cityId) {
        try {
            Integer city = Integer.valueOf(cityId);
            if (cityIdNameMap.keySet().contains(city)) {
                return cityIdNameMap.get(city);
            } else {
                return "UNDEFINED";
            }
        } catch (NumberFormatException e) {
            return "UNDEFINED";
        }
    }

    public void setPathToMap(String pathToMap) {
        this.pathToMap = pathToMap;
    }
}
